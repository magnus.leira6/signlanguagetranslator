# Assignment 2 - Sign Language Translator

## Description 
The purpose of this project was to develop a react app that had three pages
### LoginPage
The user can login with just a username and will be redirected to the profile page. 
### ProfilePage
The profile page displays the log of what you have previously translated, you can also delete the log here.
### TranslationPage
Here the user can enter words or sentences to be translated to sign language.

## Install
To download the dependencies you need to run 
***npm install***

To run the program 
***npm start***

## Author

Magnus Leira

