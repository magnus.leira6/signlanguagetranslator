
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import { useUserContext } from './contexts/UserContext';
import Login from './views/Login';
import Profile from './views/Profile';
import Translate from './views/Translate';

function App() {
  const {username} = useUserContext();
  return (
    <main className='mainContent'>
      <BrowserRouter>
      {username && <Navbar />}       
        <Routes>
          <Route path="/" element={ <Login />} />
          <Route path="/profile" element={ <Profile />} />
          <Route path="/translate" element={ <Translate />} />
        </Routes>    
      </BrowserRouter>
    </main>
  );
}

export default App;
