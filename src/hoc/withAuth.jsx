
import { Navigate } from "react-router-dom";
import { useUserContext } from "../contexts/UserContext";

const withAuth = Component => props => {
    const { username } = useUserContext();
    if(username !== "") {
        return <Component {...props} />
    }
    else {
        return <Navigate to="/" />
    }
}

export default withAuth;