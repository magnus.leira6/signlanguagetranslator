import { useResolvedPath } from "react-router-dom";
import { createHeaders } from "./headers";

const url = process.env.REACT_APP_API_URL;

//checks if user exists
const checkForUser = async (username) => {
    try {
        const response = await fetch(url+"?username="+username);
        if(!response.ok) {
            throw new Error("Something went wrong ");
        }
        const data = await response.json();
        return [null, data];
    }
    catch(error){
        return [error.message, null];
    }
}

//checks wether user exists, if not it creates a new user
export const loginUser = async (username) => {
    const [checkError, user] = await checkForUser(username);

    if(checkError !== null) {
        return [checkError, null];
    }

    if(user !== null && user.length > 0) {
        return [null, user.pop() ];
    }
   
    return await createUser(username);

    
}
//tries to create a user
const createUser = async (username) => {
    try {
        const response = await fetch(url, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        });
        if(!response.ok) {
            throw new Error(" Could not create user ");
        }

        const data = await response.json();
        return [null, data];
    }
    catch(error){
        return [error.message, null];
    }
}

//Updates the list of translations for user
export const changeTranslation = async (user, translation) => {
    try {
        const response = await fetch(`${url}/${user.id}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation],
            })
        })
        if(!response.ok){
            throw new Error("There was a problem with updating the user");
        }
        const res = await response.json();
        return [null, res];
    }
    catch(error) {
        return [error.message, null];
    }
}

//removes all translations from user
export const removeTranslationHistory = async (userId) => {
    try {
        const response = await fetch(`${url}/${userId}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [],
            })
        })
        if(!response.ok){
            throw new Error("There was a problem with updating the user");
        }
        const res = await response.json();
        return [null, res];
    }
    catch(error) {
        return [error.message, null];
    }
}