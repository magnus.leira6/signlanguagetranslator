
const apiKey = process.env.REACT_APP_API_KEY;
//specify headers for httpRequest
export const createHeaders = () => {
    return {
        "Content-Type": "application/json",
        "x-api-key": apiKey,
    }
}