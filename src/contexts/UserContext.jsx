import { createContext, useContext, useState } from "react";



export const UserContext = createContext();

export const useUserContext = () => useContext(UserContext);

export const UserProvider = (props) => {

    const [username, setUsername] = useState(sessionStorage.getItem("username") || "");
    const [translations, setTranslations] = useState(sessionStorage.getItem("translations") || "");


    const updateTranslations = (trans) => {
        sessionStorage.setItem("translations", trans);
        setTranslations(trans);
    }

    const login = (user) => {
        sessionStorage.setItem("username", user.username);
        sessionStorage.setItem("translations", user.translations);
        sessionStorage.setItem("userId", user.id)
        setUsername(user.username);
        setTranslations(user.translations);
    }

    const logout = (username) => {
        sessionStorage.removeItem("username");
        sessionStorage.removeItem("translations");
        sessionStorage.removeItem("userId");
        setUsername("");
        setTranslations("");
    }

    const state = {
        username,
        translations,
        login,
        logout,
        updateTranslations
    }

    return (
        <UserContext.Provider value={state}>
            {props.children}
        </UserContext.Provider>
    );
}