import TranslationHistoryItem from "./TranslationHistoryItem";

const ProfileTranslationHistory = ({translations}) => {

 

    const translationList = translations.split(",")
    
    return (
        <ul>
        {translationList.map((translation, index) => {
            if(translation === ""){
                return null;
            }
            return <TranslationHistoryItem key={index + "-" + translation} translation={translation}/>
        })}   
        </ul>
    );

}

export default ProfileTranslationHistory;