
const TranslateSignBox = ({word}) => {



    const wordList = word.split("");

    return (
        <div>
            <h4>Translation:</h4>
            {wordList.map((char, index) => {
                if(new RegExp(/[a-z]/).test(char)) {
                    return <img key={index} src={"individial_signs/"+char+".png"} />
                }
                else {
                    return null;
                }
            })}

        </div>
    );
}

export default TranslateSignBox;