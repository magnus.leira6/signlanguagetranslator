import { useState } from "react";
import { useForm } from "react-hook-form";
import "../../styles/Login.css";
import "../../styles/Translation.css";

const TranslateTextBox = ({ onTranslate }) => {

    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = ({ translateWord }) => onTranslate(translateWord);

    return (
        <div className="translateTextBoxDiv">
            <form className="loginForm" onSubmit={handleSubmit(onSubmit)}>
                <input className="loginFormInput" type="text" {...register("translateWord")} placeholder="word" />
                <button type="submit">Translate</button>
            </form>
        </div>
    );
}

export default TranslateTextBox;