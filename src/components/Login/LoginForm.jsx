import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useUserContext } from "../../contexts/UserContext";
import { loginUser } from "../../api/user";
import { useNavigate } from "react-router";
import "../../styles/Login.css";


const usernameConfig = {
    required: true,
    minLength: 2,
}

const LoginForm = () => {

    const { register, handleSubmit, formState: { errors }} = useForm();

    const [loading, setLoading] = useState(false);
    const [apiError, setApiError] = useState(null);
    const { username, login, logout } = useUserContext();

    const navigate = useNavigate();

    useEffect(() => {
        if(username !== ""){
            navigate("profile");
        }

    }, [username, navigate])

    const onSubmit = async ({ username }) => {
        setLoading(true);
        const [error, userResp] = await loginUser(username);
        if(error) {
            setApiError(error);
        }
        if(userResp !== null && userResp !== undefined) {
            login(userResp)
            
        }
        setLoading(false);
    }

    const errorMessage = (() => {
        if(!errors.username){
            return null;
        }
        if(errors.username.type === "required") {
            return <span> Username is required</span>
        }
        
        if(errors.username.type === "minLength"){
            return <span> Username needs to be at least 2 characters</span>
        }
    })()

    return (
            <form className="loginForm" onSubmit={handleSubmit(onSubmit)}>

                    <input className="loginFormInput" type="text" {...register("username", usernameConfig)} placeholder="What is your name?" />
                    {errorMessage}
                <button className="loginFormButton" type="submit">Login</button>
                { loading && <p>Loading</p>}
                { apiError && <p> { apiError } </p>}
            </form>
    );
}

export default LoginForm;