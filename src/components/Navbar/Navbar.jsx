import { NavLink } from "react-router-dom";
import { useUserContext } from "../../contexts/UserContext";
import "../../styles/Navbar.css"

const Navbar = () => {

    const { username, logout } = useUserContext();

    const handleLogout = () => {
        if (window.confirm("Are you sure you wish to logout")) {
            logout(username);
        }

    }

    return (

        <div className="navDiv">

            {username &&

                    <ul className="navbarUl">
                        <li className="navbarLi"><NavLink className="navbarLink" to="/translate">Translate</NavLink></li>
                        <li className="navbarLi"><NavLink className="navbarLink" to="/profile">Profile</NavLink></li>
                        <li className="navbarLiButton"><button className="navbarLink" onClick={handleLogout}>Logout</button></li>
                    </ul>
            }
        </div>
    );

}

export default Navbar;