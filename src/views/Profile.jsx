import { removeTranslationHistory } from "../api/user";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import { useUserContext } from "../contexts/UserContext";
import withAuth from "../hoc/withAuth";


const Profile = () => {

    const { username, updateTranslations } = useUserContext();


    const translationList = sessionStorage.getItem("translations");

    const handleRemoveHistory = async () => {
        const userId = sessionStorage.getItem("userId");
        const [error, user] = await removeTranslationHistory(userId);
        updateTranslations([]);
    }
    
    return (
        <div className="profileDiv">
            <h1 className="profileHeader">Translation history for {username}</h1>
            <ProfileTranslationHistory translations={translationList} />
            <button type="button" className="removeHistoryButton" onClick={handleRemoveHistory}>Remove all translations</button>
        </div>
    );
}

export default withAuth(Profile);