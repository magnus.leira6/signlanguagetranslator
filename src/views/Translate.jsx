import { useState } from "react";
import { changeTranslation } from "../api/user";
import TranslateSignBox from "../components/Translate/TranslateSignBox";
import TranslateTextBox from "../components/Translate/TranslateTextBox";
import { useUserContext } from "../contexts/UserContext";
import withAuth from "../hoc/withAuth";
import "../styles/Translation.css";

const Translate = () => {

    const [word, setWord] = useState("");
    const { updateTranslations } = useUserContext();

    const handleTranslate = async (translateWord) => {
        setWord(translateWord);

        const [errorMsg, user] = await changeTranslation({
            id: sessionStorage.getItem("userId"),
            username: sessionStorage.getItem("username"),
            translations: [sessionStorage.getItem("translations")]
        }, translateWord);
        updateTranslations(user.translations);
    }

    return (
        <div className="translateDiv">
            <h1>Translate</h1>
            <TranslateTextBox onTranslate={handleTranslate} />
            <TranslateSignBox word={word} />
        </div>
    );
}

export default withAuth(Translate);